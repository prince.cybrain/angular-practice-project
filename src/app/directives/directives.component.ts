import { Component } from '@angular/core';

@Component({
  selector: 'app-directives',
  template: `<h3>ngIF</h3>
  <div *ngIf="isTrue; else displaythis">if is working....</div>
<ng-template #displaythis>else is working.....</ng-template><br>
<div *ngIf="isLogged">
  Welcome back, {{ username }}!
</div>
<br><br>
<h3>ngFor</h3>
<div *ngFor="let shift of shifts">
    <div [style.background]="shift.color">{{ shift.color }}</div>
</div><br><br>
<div *ngFor="let item of items">
    {{ item.name }} - {{ item.price }}
</div><br>
<h3>ngForIf</h3>
<div *ngIf="items.length > 2; else displayelse">
      <div *ngFor="let item of items">
        {{ item.name }} - {{ item.price}}
      </div>
</div>
<ng-template #displayelse>item not found...</ng-template>
`,
  styleUrls: ['./directives.component.scss']
})
export class DirectivesComponent {
  isTrue:boolean=true
  isLogged:boolean=true
  username="Prince"
  shifts:any=[
     {color:'red'},
     {color:'green'},
     {color:'black'},
     {color:'purple'},
     ]
  items=[
      { name: 'Sugar', price: 10 },    { name: 'Salt', price: 12},    { name: 'Tea', price: 14 },
  ]
    
}

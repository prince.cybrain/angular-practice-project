import { Component } from '@angular/core';

@Component({
  selector: 'app-stylebinding',
  template: `
  <h2> ngClass </h2>
  <button [ngClass]="{ 'active': isActive }" (click) = "clickme()">Click me!</button>
  <p>{{work}}</p>

   <h2>ngStyle</h2>
  <div [ngStyle]="{'font-size': fontSize + 'px'}">
  The font size is {{ fontSize }}px.
</div>
`,
  styles:[`
    .active {
    background-color: yellow;
    color: black;
  }
`],
})
export class StylebindingComponent {
  isActive = false;
  fontSize = 50;
   work!:string
  clickme(){
this.work = 'Yaa, It is working'
  }
}

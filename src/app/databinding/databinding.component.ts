import { Component } from '@angular/core';

@Component({
  selector: 'app-databinding',
  template: `<h3>Interpolation</h3>
  <li>Name: {{ name }}</li>  
  <li>Email: {{ email }}</li>

  <h3>Property Binding</h3>
  <input type="text" [value]="userName">
  
  <h3>Event Binding</h3>
  <button (click)="CLICK()">ClickOnce</button>
  <h3>{{eventbinding}}</h3><br><br>
  <button (click)="Click($event.clientX, $event.clientY)">Click Again</button>
  ` ,
  styleUrls: ['./databinding.component.scss']
})
export class DatabindingComponent {
  name="prince"
  email="aa@gmail.com"
  userName:string = "Peter"; 

  eventbinding!: string;
  CLICK(){

    this.eventbinding = 'Hello,How are u'
  
   }
   Click(x: number, y: number) {
    this.eventbinding =`Button clicked at (${x}, ${y})!`
   }

}
